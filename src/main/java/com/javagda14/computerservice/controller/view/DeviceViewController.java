package com.javagda14.computerservice.controller.view;

import com.javagda14.computerservice.model.Client;
import com.javagda14.computerservice.model.Device;
import com.javagda14.computerservice.model.dto.AddDeviceToClientDto;
import com.javagda14.computerservice.service.DeviceService;
import com.javagda14.computerservice.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Optional;

@Controller
@RequestMapping("/view/device/")
public class DeviceViewController {

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private LoginService loginService;

    @GetMapping("/add")
    public String add(Model model) {
        Optional<Client> clientOptional = loginService.getLoggedInUser();
        if (!clientOptional.isPresent()) {
            // jeśli nie jesteśmy zalogowani przekieruj na panel logowania
            return "redirect:/login";
        }

        // jeśli jesteśmy logowani stworzymy sobie obiekt logowania
        AddDeviceToClientDto addDeviceToClientDto = new AddDeviceToClientDto();
        // w obiekcie przypisujemy do pola userId nasz identyfikator zalogowanego użytkownika
        addDeviceToClientDto.setUserId(clientOptional.get().getId());

        // przekazujemy obiekt do formularza (żeby go za chwilę wypełnić)
        model.addAttribute("added_device", addDeviceToClientDto);

        return "device/add_form";// zwracam nazwę html
    }

    @PostMapping("/add")
    public String add(Model model, AddDeviceToClientDto dto) {
        // weryfikacja że ID w dto jest takie samo jak Id zalogowanego użytkownika.
        // (zalecane) możecie stworzyć metodę w LoginService
        // todo: metoda w loginService (checkLoggedInId(Long user_id))
        // w wyniku zwraca boolean - czy obecnie zalogowany użytkownik ma id == user_id(z parametru)
        // jeśli zalogowany użytkownik ma poprawne id, to przechodzimy dalej.
        // w przeciwnym razie przekieruj na stronę logowania.

        Optional<Device> optionalDevice = deviceService.addDevice(dto);
        if (!optionalDevice.isPresent()) {
            model.addAttribute("added_device", dto);
            model.addAttribute("error_message", "Validation error!");
            return "device/add_form";// zwracam nazwę html
        }

        // jeśli uda się zalogować wyświetl (przekieruj na) profil użytkownika
        return "redirect:/view/client/profile/" + dto.getUserId() + "/";
    }
}
