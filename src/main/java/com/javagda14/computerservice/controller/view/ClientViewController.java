package com.javagda14.computerservice.controller.view;

import com.javagda14.computerservice.model.Client;
import com.javagda14.computerservice.model.Device;
import com.javagda14.computerservice.model.dto.AddClientDto;
import com.javagda14.computerservice.service.ClientService;
import com.javagda14.computerservice.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/view/client/")
public class ClientViewController {

    @Autowired
    private ClientService clientService;

    @Autowired
    private LoginService loginService;

//    private int licznik = 0;
//    @GetMapping("/test")
    // wartość zwracana to nazwa pliku bez rozszerzenia html w katalogu templates
//    public String getAddView(Model model) {
    // model możemy przekazać w parametrze, ale jest opcjonalny
//        model.addAttribute("licznik", licznik);
    // attribute to to samo co attribute w request (JSP)
//        return "index";
//    }


    @GetMapping("/profile")
    // model - do wysłania danych do html
    public String viewProfile(Model model) {
        Optional<Client> optionalClient = loginService.getLoggedInUser();

        if (optionalClient.isPresent()) {
            Client client = optionalClient.get();
            List<Device> deviceList = client.getDeviceList();

            // ładuję klienta i jego urządzenia do modelu (jako atrybuty)
            // żeby je za chwile wyświetlić
            model.addAttribute("client", client);
            model.addAttribute("deviceList", deviceList);

            // widok może się załadować
            return "client/profile";
        }
        // po stronie backend'u - wykorzystaj dostępne Ci serwisy i
        // wyświetl na stronie html:
        // *- dane użytkownika o podanym id (z obiektu Client)
        // - wszystkie urządzenia tego użytkownika (lista Device)

        return "redirect:/login";
    }
}
