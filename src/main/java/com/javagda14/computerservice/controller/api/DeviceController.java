package com.javagda14.computerservice.controller.api;

import com.javagda14.computerservice.model.Device;
import com.javagda14.computerservice.model.dto.AddDeviceToClientDto;
import com.javagda14.computerservice.model.dto.ModifyDeviceDto;
import com.javagda14.computerservice.service.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/device/")
public class DeviceController {
    @Autowired
    private DeviceService deviceService;

    @PostMapping("/add")
    public ResponseEntity add(@RequestBody AddDeviceToClientDto dto) {
        Optional<Device> optionalDevice = deviceService.addDevice(dto);
        if (optionalDevice.isPresent()) {
            return ResponseEntity.ok(optionalDevice.get());
        }
        return ResponseEntity.badRequest().build();
    }

    @PostMapping("/modify")
    public ResponseEntity modify(@RequestBody ModifyDeviceDto dto) {
        Optional<Device> optionalDevice = deviceService.modify(dto);
        if (optionalDevice.isPresent()) {
            return ResponseEntity.ok(optionalDevice.get());
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/list/{user_id}")
    public ResponseEntity listUserDevices(@PathVariable(name = "user_id") Long id) {
        List<Device> optionalDevice = deviceService.listUserDevices(id);
        return ResponseEntity.ok(optionalDevice);
    }

}
