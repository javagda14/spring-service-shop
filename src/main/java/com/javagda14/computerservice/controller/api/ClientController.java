package com.javagda14.computerservice.controller.api;

import com.javagda14.computerservice.model.Client;
import com.javagda14.computerservice.model.dto.AddClientDto;
import com.javagda14.computerservice.model.dto.FilterClientsDto;
import com.javagda14.computerservice.model.dto.ModifyClientDto;
import com.javagda14.computerservice.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/client/")
public class ClientController {
    @Autowired
    private ClientService clientService;

    @PostMapping("/add")
    public ResponseEntity add(@RequestBody AddClientDto dto) {
        Optional<Client> optionalClient = clientService.register(dto);
        return optionalClient.<ResponseEntity>map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.badRequest().build());
    }

    @PostMapping("/modify")
    public ResponseEntity add(@RequestBody ModifyClientDto dto) {
        Optional<Client> optionalClient = clientService.modify(dto);
        return optionalClient.<ResponseEntity>map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.badRequest().build());
    }

    @PostMapping("/filter/")
    public ResponseEntity filter(@RequestBody FilterClientsDto dto) {
        List<Client> optionalClient = clientService.filter(dto);
        return ResponseEntity.ok(optionalClient);
    }

    @GetMapping("/get/{id}")
    public ResponseEntity get(@PathVariable(name = "id") Long id) {
        Optional<Client> optionalClient = clientService.find(id);
        return optionalClient.<ResponseEntity>map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.badRequest().build());
    }

    @GetMapping("/getAll")
    public ResponseEntity getAll() {
        List<Client> optionalClient = clientService.getAll();
        return ResponseEntity.ok(optionalClient);
    }

}
