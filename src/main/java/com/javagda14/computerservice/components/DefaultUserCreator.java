package com.javagda14.computerservice.components;

import com.javagda14.computerservice.model.Client;
import com.javagda14.computerservice.model.dto.AddClientDto;
import com.javagda14.computerservice.repository.ClientRepository;
import com.javagda14.computerservice.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Optional;

@Component
@Lazy
public class DefaultUserCreator {

    @Autowired
    public DefaultUserCreator(ClientService clientService) {
        Optional<Client> clientOptional = clientService.findByEmail("4dm1n@localhost");
        if (!clientOptional.isPresent()) {
            clientService.register(
                    new AddClientDto(
                            "4dm1n@localhost",
                            "-",
                            "-",
                            "admin"));
        }
    }
}
