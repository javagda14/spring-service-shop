package com.javagda14.computerservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Device {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private Double value;
    private LocalDateTime dateAdded;

    @ManyToOne
    @JsonIgnore
    private Client owner;

    @OneToMany(mappedBy = "device")
    @JsonIgnore
    private List<ServiceTask> serviceTasks;


}
