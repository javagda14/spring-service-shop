package com.javagda14.computerservice.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AddDeviceToClientDto {
    @NotNull
    private Long userId;
    private String name;
    private Double value;
}