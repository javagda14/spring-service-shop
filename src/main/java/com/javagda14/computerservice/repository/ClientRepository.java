package com.javagda14.computerservice.repository;

import com.javagda14.computerservice.model.Client;
import com.javagda14.computerservice.model.ServiceTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.Optional;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {
    List<Client> findAllByNameContainingOrEmailContainingOrSurnameContaining(String name, String email, String surname);

    Optional<Client> findByEmail(String email);
}
