package com.javagda14.computerservice.repository;

import com.javagda14.computerservice.model.Device;
import com.javagda14.computerservice.model.ServiceTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.web.JsonPath;

import java.util.List;

public interface ServiceTaskRepository extends JpaRepository<ServiceTask, Long> {
    List<ServiceTask> findAllByDeviceIn(List<Device> devices);
}
