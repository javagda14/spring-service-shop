package com.javagda14.computerservice.repository;

import com.javagda14.computerservice.model.Client;
import com.javagda14.computerservice.model.Device;
import com.javagda14.computerservice.model.ServiceTask;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DeviceRepository extends JpaRepository<Device, Long> {
    List<Device> findAllByOwner(Client client);
}
