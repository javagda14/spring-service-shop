package com.javagda14.computerservice.service;

import com.javagda14.computerservice.components.DefaultUserCreator;
import com.javagda14.computerservice.model.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.validation.constraints.Email;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service(value = "loginService")
public class LoginService implements UserDetailsService {

    @Autowired
    private ClientService clientService;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private DefaultUserCreator defaultUserCreator;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<Client> clientOptional = clientService.findByEmail(email);
        if (clientOptional.isPresent()) {
            // poprawne logowanie
            Client client = clientOptional.get();

            if (client.getEmail().equals("admin@localhost")) {
                return User.withUsername(client.getEmail()).password(client.getPassword()).roles("ADMIN", "USER").build();
            }
            return User.withUsername(client.getEmail()).password(client.getPassword()).roles("USER").build();
        }
        // brak użytkownika z podanym mailem!
        //
        throw new UsernameNotFoundException("User not found by name: " + email);
    }

    public Optional<Client> getLoggedInUser() {
        if (SecurityContextHolder.getContext().getAuthentication() == null ||
                SecurityContextHolder.getContext().getAuthentication().getPrincipal() == null ||
                !SecurityContextHolder.getContext().getAuthentication().isAuthenticated()) {
            // nie jesteśmy zalogowani
            return Optional.empty();
        }

        if (SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof User) {
            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return clientService.findByEmail(user.getUsername());
            // jesteśmy zalogowani, zwracamy user'a
        }

        return Optional.empty();
    }
}
