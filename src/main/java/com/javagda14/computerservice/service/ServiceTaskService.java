package com.javagda14.computerservice.service;

import com.javagda14.computerservice.model.Client;
import com.javagda14.computerservice.model.Device;
import com.javagda14.computerservice.model.ServiceTask;
import com.javagda14.computerservice.model.dto.AddServiceTaskDto;
import com.javagda14.computerservice.repository.ClientRepository;
import com.javagda14.computerservice.repository.DeviceRepository;
import com.javagda14.computerservice.repository.ServiceTaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ServiceTaskService {
    @Autowired
    private ServiceTaskRepository serviceTaskRepository;

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private ClientRepository clientRepository;

    public Optional<ServiceTask> add(AddServiceTaskDto dto) {
        Optional<Device> deviceOptional = deviceRepository.findById(dto.getDeviceId());
        if (deviceOptional.isPresent()) {
            Device device = deviceOptional.get();

            ServiceTask serviceTask = new ServiceTask();
            serviceTask.setDateAdded(LocalDateTime.now());
            serviceTask.setDescription(dto.getDescription());
            serviceTask.setDevice(device);

            serviceTask = serviceTaskRepository.save(serviceTask);
            device.getServiceTasks().add(serviceTask);

            device = deviceRepository.save(device);
            return Optional.of(serviceTask);
        }
        return Optional.empty();
    }

    public List<ServiceTask> getAllUserServiceTasks(Long user_id) {
        Optional<Client> clientOptional = clientRepository.findById(user_id);
        if (clientOptional.isPresent()) {
            Client client = clientOptional.get();
            List<Device> deviceList = deviceRepository.findAllByOwner(client);

            return serviceTaskRepository.findAllByDeviceIn(deviceList);
        }
        return new ArrayList<>();
    }

    public List<ServiceTask> getAll() {

        return serviceTaskRepository.findAll();
    }
}
